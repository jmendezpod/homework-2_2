package ru.tsystems.devschool.api;

import org.springframework.stereotype.Controller;
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2023-11-24T23:01:55.527+01:00")

@Controller
public class ApiApiController implements ApiApi {

    private final ApiApiDelegate delegate;

    @org.springframework.beans.factory.annotation.Autowired
    public ApiApiController(ApiApiDelegate delegate) {
        this.delegate = delegate;
    }

    @Override
    public ApiApiDelegate getDelegate() {
        return delegate;
    }
}
